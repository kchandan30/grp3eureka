FROM openjdk:8
WORKDIR /playbook/eurekaproject/sample-eureka/eureka-server/target/
#COPY  /playbook/eurekaproject/sample-eureka/eureka-server/target/eureka-server-0.0.1-SNAPSHOT.jar  /playbook
EXPOSE 8761
ENTRYPOINT  ["java","-jar","eureka-server-0.0.1-SNAPSHOT.jar"]
